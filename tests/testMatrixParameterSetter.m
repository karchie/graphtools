function test_suite = testMatrixParameterSetter
% testMatrixParameterSetter test for matrix_parameter_setter
% Author: Kevin A. Archie <karchie@wustl.edu>
initTestSuite;

function testMPS_thr
[subjects thresholds numanalysis xs] = ngt.matrix_parameter_setter('prmtest.prm', 'thr');
p = ngt.load_prm('prmtest.prm');
assertEqual([p.loend:p.step:p.hiend]', thresholds)
d = size(thresholds);
assertEqual(d(1), numanalysis);
assertEqual(repmat([p.subjectA p.subjectZ], [numanalysis 1]), subjects);
assertEqual(thresholds, xs);

function testMPS_box
p = ngt.load_prm('prmtest.prm');
[subjects thresholds numanalysis xs] = ngt.matrix_parameter_setter(p, 'box');
begins = [p.subjectA:p.boxcarstep:p.subjectZ]';
ends = begins+p.boxcarsize;
while (ends(end,1) > p.subjectZ)
    begins(end,:) = [];
    ends(end,:) = [];
end
assertEqual(begins, xs);
assertEqual([begins ends], subjects);
d = size(subjects);
assertEqual(repmat(p.threshold,[d(1) 1]), thresholds);

function testMPS_other
assertExceptionThrown(@() ngt.matrix_parameter_setter('prmtest.prm', 'foo'),...
    'matrix_parameter_setter:badAnalysisType');
