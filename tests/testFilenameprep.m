function test_suite = testFilenameprep
% testFilenameprep Unit tests for filenameprep
% Author: Kevin A. Archie <karchie@wustl.edu>
initTestSuite;

function test_thr_r
p = ngt.load_prm('prmtest.prm');
p.thresholdtype='r';
stem = ngt.filenameprep(p, 'thr');
assertEqual('Tr01to003in-001_S1to53', stem);

function test_box_kden
p = ngt.load_prm('prmtest.prm');
p.thresholdtype='kden';
stem = ngt.filenameprep(p, 'box');
assertEqual('Tk01_S1to53_BX1st1s', stem);

function test_thr_abs
p = ngt.load_prm('prmtest.prm');
p.thresholdtype='abs';
stem = ngt.filenameprep(p, 'thr');
assertEqual('Tabs01to003in-001_S1to53', stem);
