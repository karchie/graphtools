function test_suite = testDotRemover
% testDotRemover test for dotremover
% Author: Kevin A. Archie <karchie@wustl.edu>
initTestSuite;

function testDR
assertEqual('foobar', ngt.dotremover('foo.bar'))
assertEqual('abcdef', ngt.dotremover('.a.bc.d.ef'))
assertEqual('', ngt.dotremover('...'))

function testDR_nochange
assertEqual('', ngt.dotremover(''));
assertEqual('foo', ngt.dotremover('foo'));
assertEqual('x4df^#', ngt.dotremover('x4df^#'))

