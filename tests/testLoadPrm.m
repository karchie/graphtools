function test_suite = testLoadPrm
% testLoadPrm Unit test for load_prm
% Author: Kevin A. Archie <karchie@wustl.edu>
initTestSuite;

function testLoadParamsStruct
p.foo = 'foo';
assertTrue(isequal(p, ngt.load_prm(p)));

function testLoadParamsString
pf = 'prmtest.prm';
p = ngt.load_prm(pf);
assertFalse(isequal(pf, p));
assertEqual('stem_name', p.stemname);
assertEqual(1, p.boxcarsize);

function testLoadParamsOther
assertExceptionThrown(@() ngt.load_prm([]), 'load_prm:badArgument');
assertExceptionThrown(@() ngt.load_prm(2.3), 'load_prm:badArgument');
