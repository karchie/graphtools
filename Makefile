# Makefile for graphtools
# Copyright (c) 2011 Washington University
# Author: Kevin A. Archie <karchie@wustl.edu>

PROJECT=graphtools
VERSION ?= $(shell git describe --tags HEAD | sed -e 's/^v//g')

# Used for target update.tgz; must define PREV_COMMIT for that target
CHANGES=$(shell git diff --numstat ${PREV_COMMIT} | awk '$$3!="Makefile" {print $$3}')

DOCFILES=graphtools.txt bigRGBpalette.txt printsamplegraphfiles.txt \
	prmformat.txt COPYRIGHT LICENSE

MFILES = assortativity.m BCEBC.m bootstrapper_tally.m \
	bu_clusteringcoefficients.m bu_pathlengths.m dotremover.m \
	edge_betweenness_bin.m efficiency.m euclidean_distance.m \
	fcimage_2dviewer.m filenamefinder.m filenameprep.m graphcluster.m \
	graphperturbation.m graphtools.m infomap_wrapper.m \
	listcombiner.m load_prm.m loRAM_graphcluster.m mat2pajek.m \
	matfile_loader.m matrix_former.m matrix_normalize.m \
	matrix_parameter_setter.m matrixrgbmapper.m \
	matrix_thresholder.m matrix_xdistance.m M_calc_modularity.m \
	M_caretfilemaker.m modify_clrfile.m modularity_und.m \
	M_visuals.m netmet_randomgraphmetrics.m network_metrics.m \
	node2module_matrix.m pairtallyer.m printsamplefiles.m \
	quickprmfile.m quickroifile.m randmio_und.m rawoutput2clr.m \
	rgbmapper.m roifilereader.m similarity_indices.m siteconfig.m \
	soniawriter.m tiffmaker.m

MFILEDIR=+ngt

TESTDIR=tests

VPATH=$(MFILEDIR)


all: dist

.PHONY: clean dist dist-dir dist-doc dist-m update.tgz

update.tgz:
	-rm -rf $(PROJECT) $(PROJECT)-$(VERSION)-update.tgz
	mkdir $(PROJECT)
	tar c $(CHANGES) | tar -C $(PROJECT) -x
	echo $(VERSION) >$(PROJECT)/VERSION
	tar cvzf $(PROJECT)-$(VERSION)-update.tgz $(PROJECT)
	-rm -rf $(PROJECT)

dist: dist-dir dist-doc dist-m
	tar cvzf $(PROJECT)-$(VERSION).tgz $(PROJECT)-$(VERSION)

dist-dir:
	-rm -rf $(PROJECT)-$(VERSION) $(PROJECT)-$(VERSION).tgz
	mkdir $(PROJECT)-$(VERSION)
	echo >$(PROJECT)-$(VERSION)/VERSION $(VERSION)
	mkdir $(PROJECT)-$(VERSION)/$(MFILEDIR)


dist-doc: $(DOCFILES)
	for h in $^; do \
		cp $$h $(PROJECT)-$(VERSION) ;\
	done


dist-m: $(MFILES)
	for h in $^; do \
		cp $$h $(PROJECT)-$(VERSION)/$(MFILEDIR) ;\
	done


clean:
	rm -f *~ $(MFILEDIR)/*~ $(TESTDIR)/*~ $(PROJECT)-$(VERSION).tgz
	rm -rf $(PROJECT)-$(VERSION)
