function assort = assortativity(mat)
% ASSORTATIVITY Finds the degree assortativity of a binary undirected
% graph.
%
%    a = ngt.assortativity(m) returns the degree assortativity of the binary
%    undirected graph m.
%
% Basically, all this is doing is finding the remaining degree of the nodes
% at either end of every edge (degree-1), and computing the correlation
% across all of these pairs of values (for each edge).
%
% Author: jdp 10/10/10

% remove the diagonal and ensure binary
mat=mat.*(~eye(size(mat,1)));
mat=double(logical(mat));

% find the degree of each node
degrees=sum(mat);

% find all edges
[x y]=find(triu(mat,1));

% get the residual degree at each edge, calculate the correlation
b(:,1)=degrees(x);
b(:,2)=degrees(y);
b=b-1;
c=corrcoef(b);
assort=c(1,2);
