function printsamplefiles()
% PRINTSAMPLEFILES Generate sample ROI and params files for graphtools
%
% display the explanation for the printed files
type printsamplegraphfiles.txt

% print a sample roifile
fid=fopen('sampleroifile.roi','w');
fprintf(fid,'ID X Y Z NAME CATA COLORA CATB COLORB\n');
fprintf(fid,'1 3 -7 6 PCC Brain Black Brain Black\n');
fprintf(fid,'2 6 2 2 ACC Brain Black Brain Black\n');
fprintf(fid,'3 9 -1 -3 pFC Brain Black Brain Black\n');
fclose(fid);


% print a sample prmfile
fid=fopen('sampleprmfile.prm','w');
fprintf(fid,'matrix.mat\n');
fprintf(fid,'roifile.roi\n');
fprintf(fid,'finalanalysis\n');
fprintf(fid,'1\n');
fprintf(fid,'26\n');
fprintf(fid,'0\n');
fprintf(fid,'0.01\n');
fprintf(fid,'0.2\n');
fprintf(fid,'/data/stete/networkanalysis/\n');
fprintf(fid,'0.1 \n');
fprintf(fid,'10\n');
fprintf(fid,'2\n');
fprintf(fid,'kden\n');
fclose(fid);






