function p = load_prm(params)
% LOAD_PRM Returns the requested parameters structure.
%    ps = ngt.load_prm(p) returns a parameters structure. If p is a string,
%    load_prm assumes this is a filename and loads the parameters structure
%    from the named .prm file. If p is a structure, load_prm returns the
%    same structure, unchanged. Any other input is an error.
%
%    For details about the .prm data format, do: type prmformat.txt
% AUTHOR: Kevin A. Archie <karchie@wustl.edu>
%   (adapted from Jonathan Power's prmfilereader)
if isstruct(params)
  p = params;
elseif ischar(params)
  pcells=textread(params,'%s');  % read the prmfile
  p.prmfile = params;
  p.matfile=pcells{1,1};
  p.roifile=pcells{2,1};
  p.stemname=pcells{3,1};
  p.subjectA=str2double(pcells{4,1});
  p.subjectZ=str2double(pcells{5,1});
  p.loend=str2double(pcells{6,1});
  p.step=str2double(pcells{7,1});
  p.hiend=str2double(pcells{8,1});
  p.writepath=pcells{9,1};
  p.threshold=str2double(pcells{10,1});
  p.boxcarsize=str2double(pcells{11,1});
  p.boxcarstep=str2double(pcells{12,1});
  p.thresholdtype=pcells{13,1};
else
  error('load_prm:badArgument', ...
      'argument to load_prm must be parameters structure or file name')
end
