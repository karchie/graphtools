function graphperturbation(clustertype,prm,analysistype,savematfile,xdistance,vloend,vstep,vhiend,randomreps)
% This script measures how resistant community assignments are to a
% perturbation. It takes a graph over a series of analyses as described by
% the prm and analysistype, and for each of those analyses, creates
% randomreps random graphs. These random graphs, along with the real graph,
% are perturbed by randomly rewiring the graph, and the similarity of the
% original graphs to the perturbed graphs is measured with variation of
% information and normalized mutual information. A range of perturbation
% (alpha - % opportunity of rewiring an edge) is provided by the user, and
% the output is a series of pictures of the NMI and VI over the alpha range
% at each analysis
%
% USAGE: ngt.graphperturbation(clustertype,prmfile,analysistype,savematfile,xdistance,alphaloend,alphastep,alphahiend,randomreps)
% USAGE: ngt.graphperturbation('modularity','modbox.prm','thr',1,15,0,0.01,0.15,50)
% USAGE: ngt.graphperturbation('infomap','modbox.prm','box',0,[],0,0.05,0.25,100)
%
% Author: jdp 10/10/10

clf;

% set perturbation range and tag for output
ialpha=vloend:vstep:vhiend;
alphaend=size(ialpha,2);
alphatag=ngt.dotremover(sprintf('_grperturb_%gto%gin%gx%g', vloend, vhiend, vstep, randomreps));

prm = ngt.load_prm(prm);
if ~exist(prm.writepath, 'file')
    mkdir(prm.writepath);
end

% create filebase for output
filestem = ngt.filenameprep(prm,analysistype);

% determine the # thresholds, etc. for the analysis
[subjectarray thresholdarray numanalyses unused] = ngt.matrix_parameter_setter(prm,analysistype);

% load the matrix
[matrix unused unused] = ngt.matfile_loader(prm.matfile);

% apply a distance exclusion?
if ~isempty(xdistance)
    if isnumeric(xdistance) && (xdistance>=0)
        matrix = ngt.matrix_xdistance(matrix,prm.roifile,xdistance);
        xd=ngt.dotremover(num2str(xdistance));
        filestem=[filestem '_xd' xd ];
    else
        error('xdistance is not >=0 or is not numeric.\n');
    end
end

% create an output directory if needed
filestem=[prm.stemname '_' filestem '_BI'];
switch clustertype
    case 'modularity'
        outdir=[prm.writepath '/' filestem alphatag '_MDLRTY' ];
    case 'infomap'
        outdir=[prm.writepath '/' filestem alphatag '_INFMAP' ];
    otherwise
        error('Need to use ''modularity'' or ''infomap'' as clustering algorithms');
end
if ~exist(outdir, 'file')
    mkdir(outdir);
end

% check savematfile
switch savematfile
    case 0
    case 1
    otherwise
        error('savematfile should be ''0'' (no .mat saved) or ''1'' (.mat saved) ');
end

% initialize variables
jc=zeros(numanalyses,randomreps,alphaend,'single');
jd=jc;
ri=jc;
vi=jc;
nmi=jc;
rand_jc=jc;
rand_jd=jc;
rand_ri=jc;
rand_vi=jc;
rand_nmi=jc;


% do analyses at each threshold/boxcar
for i=1:numanalyses
    
    % get real matrix and modules
    rmat = ngt.matrix_former(matrix,subjectarray(i,1),subjectarray(i,2),'2D','diagout');
    [rmat unused unused] = ngt.matrix_thresholder(rmat,thresholdarray(i,1),prm.thresholdtype);
    rmat = single(0 < rmat);
    clrs = getmodules(rmat,clustertype,prm.roifile,[outdir '/temp' num2str(round(rand*1000))]);
    
    for j=1:randomreps
        fprintf('Thr/box %d, randomrep %d\n',i,j);
        
        % get a randommatrix and its modules
        randgraph = ngt.randmio_und(rmat,1,1);
        randclrs = getmodules(randgraph,clustertype,prm.roifile,[outdir '/temp' num2str(round(rand*1000))]);
        
        % now go through a range of perturbations
        for k=1:alphaend
            
            % perturb the real and random graphs and get modules
            permrmat = ngt.randmio_und(rmat,ialpha(k),1);
            permrandgraph = ngt.randmio_und(randgraph,ialpha(k),1);
            
            permclrs = getmodules(permrmat,clustertype,prm.roifile,[outdir '/temp' num2str(round(rand*1000))]);
            permrandclrs = getmodules(permrandgraph,clustertype,prm.roifile,[outdir '/temp' num2str(round(rand*1000))]);
            
            [jc(i,j,k) jd(i,j,k) ri(i,j,k) vi(i,j,k) nmi(i,j,k)] = ngt.similarity_indices(clrs,permclrs);
            [rand_jc(i,j,k) rand_jd(i,j,k) rand_ri(i,j,k) rand_vi(i,j,k) rand_nmi(i,j,k)] = ngt.similarity_indices(randclrs,permrandclrs);
        end
    end
end

% calculate mean and std of the information theoretic measures
mean_nmi=squeeze(mean(nmi,2))';
mean_rand_nmi=squeeze(mean(rand_nmi,2))';
std_nmi=squeeze(std(nmi,[],2))';
std_rand_nmi=squeeze(std(rand_nmi,[],2))';

mean_vi=squeeze(mean(vi,2))';
mean_rand_vi=squeeze(mean(rand_vi,2))';
std_vi=squeeze(std(vi,[],2))';
std_rand_vi=squeeze(std(rand_vi,[],2))';


% write plots of NMI and VI at each analysis
for i=1:numanalyses
    subplot(1,2,1);
    plot(ialpha,mean_nmi(:,i),'g.',ialpha,mean_rand_nmi(:,i),'r.');
    hold on; errorbar(ialpha,mean_nmi(:,i),std_nmi(:,i),'g.'); hold off;
    hold on; errorbar(ialpha,mean_rand_nmi(:,i),std_rand_nmi(:,i),'r.'); hold off;
    xlabel('alpha (randomization parameter'); ylabel('normalized mutual information'); ylim([0 1]);
    
    subplot(1,2,2);
    plot(ialpha,mean_vi(:,i),'g.',ialpha,mean_rand_vi(:,i),'r.');
    hold on; errorbar(ialpha,mean_vi(:,i),std_vi(:,i),'g.'); hold off;
    hold on; errorbar(ialpha,mean_rand_vi(:,i),std_rand_vi(:,i),'r.'); hold off;
    xlabel('alpha (randomization parameter'); ylabel('variation of information'); ylim([0 1]);
    saveas(gcf,[outdir '/' filestem '_col' num2str(i) '.tiff'],'tiff');
end

if savematfile
    save([outdir '/' filestem '.mat']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [clrs] = getmodules(rmat,clustertype,roifile,tempname)

switch clustertype
    case 'modularity'
        [clrs unused]=ngt.modularity_und(rmat);
    case 'infomap'
        pajekfile = [ tempname '.net' ];
        clrs = ngt.infomap_wrapper(roifile,rmat,pajekfile,100,1);
end
