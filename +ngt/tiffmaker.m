function mat = tiffmaker(outputname,mat,varargin)
% TIFFMAKER Produces a TIFF graphics file from a matrix.
%    TIFFMAKER produces a color view of a matrix. 'jet' in 256 colors is
%    the default map, but if the user passes in a colormap and number of
%    colors to use, these will be applied instead. Files or matrices can
%    be passed in for the matrix. The .tiff will be written to outputname
%
% USAGE: ngt.tiffmaker(outputname,inputmatrix,*colormap,numclrs*)
% USAGE: ngt.tiffmaker(outputname,inputmatrix,*colormap,numclrs*)
% USAGE: ngt.tiffmaker(outputname,inputmatrix,*colormap,numclrs*)
%
% Author: jdp 10/10/10

% set default clrmap and let user define alternate
clrmap='jet';
numclrs=256;
if ~isempty(varargin)
    if length(varargin) < 2
        error('must provide clrmap and numclrs');
    end
    clrmap=varargin{1,1};
    numclrs=varargin{1,2};
end

% load the matrix if it wasn't passed in as an array
if ~isnumeric(mat)
    mat=load(mat);
end

% convert it to an image
mat=mat2gray(mat);
mat=gray2ind(mat,256);
mat=ind2rgb(mat,eval([clrmap '(' num2str(numclrs) ')' ]));
mat=imresize(mat,10,'nearest');
imwrite(mat,outputname,'tiff');
close;
