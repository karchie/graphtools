%% Site configuration for ngt graphtools
%% Modify this file for use at your site.

% Default base image for fcimage_2dviewer
% This needs to be set only if you use fcimage_2dviewer
fcimage_base_default = '/data/cn3/SMintegration/bin/bin_referencefiles/TRIO_KY_NDC_333.4dfp.img';

% Name of infomap executable (see README)
infomap_command = 'infomap';

% Should a machine suffix be appended to the infomax executable name?
infomap_use_machine_suffix = true;
