function rgbmat = matrixrgbmapper(mat,rgbfile,matbymat)
% MATRIXRGBMAPPER Maps a matrix into rgb values
%
% This maps a matrix into rgb values (specified by the rgbfile), either the
% entire matrix at once, or at each step of the 3rd dimension
%
% mat: a matrix of numbers
% rgbfile: a MATLAB colormap, e.g. 'jet','hsv', etc.
% matbymat: 0=do entire matrix at once, 1=scale each 3rd dim separately
%
% USAGE: rgbmat = ngt.matrixrgbmapper(mat,rgbfile,matbymat)
% USAGE: rgbmat = ngt.matrixrgbmapper(mat,'jet',1)
%
% AUTHOR: jdp 10/10/10

d=size(mat);

if size(mat,3)>1
    if matbymat==0
        numcolors=nnz(unique(mat));
        mat=reshape(mat,[d(1) (d(2)*d(3))]);
        [rgbmat]=makergb(mat,numcolors,rgbfile);
        rgbmat=reshape(rgbmat,[d(1) d(2) d(3) 3]);
    elseif matbymat==1
        for i=1:d(3)
            numcolors=nnz(unique(mat(:,:,i)));
            rgbmat(:,:,i,:)=makergb(mat(:,:,i),numcolors,rgbfile);
        end
    end
else
    numcolors=nnz(unique(mat));
    [rgbmat]=makergb(mat,numcolors,rgbfile);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rgb=makergb(mat,numcolors,rgbfile)
rgb=mat2gray(mat);
rgb=gray2ind(rgb,numcolors);
rgb=ind2rgb(rgb,eval(sprintf('%s(%g)', rgbfile, numcolors)));
rgb=ceil(rgb*255);

