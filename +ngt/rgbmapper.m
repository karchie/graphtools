function [rgb shape border modulecolors moduleborders] = rgbmapper(matrix,bycolumns,rgbfile)
% RGBMAPPER Assigns rgb values, borders, and shapes to a clrmatrix
%
% This takes a clrmatrix and assigns rgb values to that matrix. It also
% specifies node borders, caret shapes, etc for standard output. It has the
% option of working with the entire clrmatrix at once, or scaling
% everything to the values within a column (useful, for example, if you
% wish to scale some parameter that has extreme values at only some
% thresholds)
%
% The variables passed in are:
%   matrix: the community assignment matrix
%   bycolumns: scale within column (1=yes, 0=no)
%   rgbfile: a string, which can be any MATLAB colormap, or a file with 3
%   columns of (0-255) RGB values. If something else is passed in, a
%   default custom color file is selected.
%
% The output variables are:
%   rgb: a [nodes x columns x 3] set of 0-255 RGB values
%   shape: [nodes x columns] one of the 7 shapes Caret can support
%   border: [nodes x columns] same idea as moduleborders
%   modulecolors: a [community x 3] set of 0-255 RGB values
%   moduleborders:  a [community x 1] cell array of colors: default is
%       black, but other colors will indicate cycles through the possible
%       color schemes (distinguishes modules in SoNIA)
%
% USAGE: [rgb modulecolors moduleborders shape border] = ngt.rgbmapper(matrix,bycolumns,rgbfile)
% USAGE: [rgb modulecolors moduleborders shape border] = ngt.rgbmapper(clrs,0,'summer')
% USAGE: [rgb modulecolors moduleborders shape border] = ngt.rgbmapper(clrs,1,'mycolors.txt')
% USAGE: [rgb modulecolors moduleborders shape border] = ngt.rgbmapper(clrs,1,[255 255 128])
%
% NOTE: 7/14/11: fixed bug in column-by-column coloring
%
% AUTHOR: jdp 10/10/10

% initialize pertinent variables
[a b]=size(matrix);
shape = cell(a,b);
shape(:)={'SPHERE'};
border = cell(a,b);
border(:)={'black'};
colors=unique(matrix);
numcolors=size(colors,1);
modulecolors=zeros(numcolors,3);
moduleborders=cell(numcolors,1);
moduleborders(:)={'Black'};
shapeoptions=7;
rgb=zeros(a,b,3);

if isnumeric(rgbfile) % if a user-supplied rgb array
    
    if size(rgbfile,2)~=3
        error('You need to supply rgb values like [0 128 255]');
    end
    
    palette=rgbfile;
    
    [rgb shape border modulecolors moduleborders] = custompalette(matrix,palette,shapeoptions,numcolors);
    
else % if file or MATLAB color palette
    
    switch rgbfile
        
        % if using one of the pre-defined color palettes in MATLAB
        case {'hsv','hot','gray','bone','copper','pink','white','flag','lines','colorcube','vga','jet','prism','cool','autumn','spring','winter','summer'}
            
            if bycolumns % if we want colors to be scaled within each column
                matrix=matrix_normalize(matrix);
                rgb=mat2gray(matrix);
                rgb=gray2ind(rgb,numcolors);
                rgb=ind2rgb(rgb,eval([rgbfile '(' num2str(numcolors) ')' ]));
                rgb=ceil(rgb*255);
                fprintf('Modulecolors and moduleborders will be left as zeros - I presume you must be using some sort of metric to color things, not module assignments.\n');
                [shape border modulecolors moduleborders] = defaultpalette(matrix);
            
            else % if we want colors scaled across the entire matrix
                if numcolors>65536 % this is the limit to gray2ind (for double)
                    numcolors=65536;
                end
                rgb=mat2gray(matrix);
                rgb=gray2ind(rgb,numcolors);
                rgb=ind2rgb(rgb,eval([rgbfile '(' num2str(numcolors) ')' ]));
                rgb=ceil(rgb*255);
                
                [shape border modulecolors moduleborders] = defaultpalette(matrix);
                
                % now find the color assigned to each value
                for i=1:numcolors
                    [xpos ypos]=find((matrix==colors(i)),1);
                    modulecolors(i,:)=rgb(xpos,ypos,:);
                end
            end
            

        otherwise % if calling a file with a user-defined palette of 3 columns of RGB values (0-255)
            
            % set the default palette, or load in the provided palette
            palettefile='bigRGBpalette.txt';
            if exist(rgbfile)
                palettefile=rgbfile;
            end
            
            % load the custom palette
            palette=load(palettefile);
            
            [rgb shape border modulecolors moduleborders] = custompalette(matrix,palette,shapeoptions,numcolors);
    end
end

% for compactness
rgb=single(rgb);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rgb shape border modulecolors moduleborders] = custompalette(matrix,palette,shapeoptions,numcolors)

[a b]=size(matrix);
shape = cell(a,b); shape(:)={'SPHERE'};
border = cell(a,b); border(:)={'black'};
colors=unique(matrix);
numcolors=size(colors,1);
modulecolors=zeros(numcolors,3);
moduleborders=cell(numcolors,1); moduleborders(:)={'Black'};
shapeoptions=7;
rgb=zeros(a,b,3);

[c d] = size(palette);

% cycle through each unique clr/shape/border option
for i=1:shapeoptions
    for j=1:c
        if numcolors<=(shapeoptions*c) % if there are enough colors to uniquely specify all assignments
            
            % the indexth color assigned
            index=(i-1)*c+j;
            
            if index<=numcolors
                
                % find the corresponding unique module in the matrix
                clear x; clear y;
                [x y]=find(matrix==colors(index));
                
                % put the unique color in that position, put it in the
                % modulecolor matrix as well
                for k=1:size(x,1)
                    rgb(x(k),y(k),1)=palette(j,1);
                    rgb(x(k),y(k),2)=palette(j,2);
                    rgb(x(k),y(k),3)=palette(j,3);
                end
                modulecolors(index,1)=palette(j,1);
                modulecolors(index,2)=palette(j,2);
                modulecolors(index,3)=palette(j,3);
                
                % and now put shapes, borders, etc on those modules
                for k=1:size(x,1)
                    if i==1
                        shape{x(k),y(k)}='SPHERE';
                        border{x(k),y(k)}='Black';
                        moduleborders{index}='Black';
                    elseif i==2
                        shape{x(k),y(k)}='DIAMOND';
                        border{x(k),y(k)}='LightGray';
                        moduleborders{index}='LightGray';
                    elseif i==3
                        shape{x(k),y(k)}='BOX';
                        border{x(k),y(k)}='Red';
                        moduleborders{index}='Red';
                    elseif i==4
                        shape{x(k),y(k)}='RING';
                        border{x(k),y(k)}='Green';
                        moduleborders{index}='Green';
                    elseif i==5
                        shape{x(k),y(k)}='DISK';
                        border{x(k),y(k)}='Orange';
                        moduleborders{index}='Orange';
                    elseif i==6
                        shape{x(k),y(k)}='SQUARE';
                        border{x(k),y(k)}='Blue';
                        moduleborders{index}='Blue';
                    elseif i==7
                        shape{x(k),y(k)}='POINT';
                        border{x(k),y(k)}='Magenta';
                        moduleborders{index}='Magenta';
                    end
                end
            end
        else
            error('Not enough rgb values to color things uniquely! Try a MATLAB palette.');
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if the rgb palette is set from a MATLAB palette, there will be no
% repetitions of colors, and foci will all be circles, all borders will be
% black for sonia nodes

function [shape border modulecolors moduleborders] = defaultpalette(matrix)

[a b]=size(matrix);
shape = cell(a,b); shape(:)={'SPHERE'};
border = cell(a,b); border(:)={'black'};
colors=unique(matrix);
numcolors=size(colors,1);
modulecolors=zeros(numcolors,3);
moduleborders=cell(numcolors,1); moduleborders(:)={'Black'};
