function [matrix nodes subjects] = matfile_loader(matfile)
% MATFILE_LOADER Reads a single variable matrix from a .mat file
%
% This script opens a .mat file containing a single variable (with any
% name), and returns that variable as the 'single' variable matrix.
%
% USAGE: [matrix nodes subjects] = ngt.matfile_loader(matfile)
%
% AUTHOR: jdp 10/10/10

% load matrix
s=load(matfile);
n=fieldnames(s);
n=char(n);
n=[ 's.' n ];
matrix=eval(n);
clear s n;
matrix=single(matrix);

nodes=size(matrix,1);
subjects=size(matrix,3);
