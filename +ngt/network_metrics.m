function network_metrics(params,analysistype,xdistance,makebinary,savematfile,netmetswitch,varargin)
% NETWORK_METRICS Calculates many properties of graphs.
% There are 3 main analysis types: simple, histograms, and smallworld.
%
% Simple is fast and should give you a decent idea where to set the bounds
% of your analysis based on fragmentation of a graph into components, and
% the edge density observed. Histograms makes cdfs and histograms of the
% strengths in the matrix, and it also plots the edge weights against
% Euclidean distance. Smallworld is a workhorse and is considerably slower,
% calculating many network properties such as smallworld parameters.
%
%%%%%%%% A WORD ABOUT .MAT FILES %%%%%%%%
%
% If you save a .mat file, the variables are self-descriptively named. If
% you are not bootstrapping, most variables will be columns with entries
% corresponding to each analysis. Some matrices (e.g. bucc) will be nodes x
% analyses.
%
% If you bootstrapped, these variables will have another dimension, which
% is the bootstrapping dimension. Averaging across this dimension will
% yield the means and stds that are output in the bootstrapping .tiffs and
% .txt files.
%
%%%%%%%% IMPORTANT VARIABLES IN .MAT FILES %%%%%%%%
%
% Thr/Box: the threshold/boxcar of the analysis
% N: number of nodes in graph
% K: number of edges in the graph
% kden: percent of edges present out of those that are possible
% k: avg. number of edges per node
% lnN: log(10) of the number of nodes (when k falls below this, distrust smallworld lattice calculations)
% components: how many pieces is the graph in (this is 99.99% accurate, but sometimes not)
% giantcomponent: the fraction of nodes in the largest component
% graphconnectedness: the mean of an NxN matrix showing whether nodes can reach one another
% eglobal: global efficiency (Latora and Marchiori)
% elocal: local efficiency
% randomeglobal: in a random matrix
% randomelocal: in a random matrix
% CC: observed average local clustering coefficient (Watts & Strogatz)
% L: observed characteristic path length (Watts & Strogatz)
% latticeCC: CC of an equivalent lattice
% latticeL: L of an equivalent lattice
% randomCC: CC of an equivalent random graph
% randomL: L of an equivalent random graph
% n_XX: the variable normalized by the lattice values (the highest values possible)
% lambda: L/randomL
% gamma: CC/randomCC
% sigma: lambda/gamme
% assortativity: degree assortativity (Newman, 2002)
%
%
%%%%%%%% HOW TO USE THIS SCRIPT %%%%%%%%%
%
% USAGE: network_metrics(params,analysistype,xdistance,makebinary,savematfile,net metswitch,*randomreps*,**#bootstraps,bootstrapsamplesize**)
%
% params: the standard graphtools parameters (struct or prmfile path)
% analysistype: 'thr' or 'box' as usual
% xdistance: discard edges of nodes within xdistance mm of one another
%   **enter [] to not apply any distance exclusion**
% makebinary: 1 to make the graph binary, 0 to leave it weighted
%   **smallworld requires a binary graph**
% savematfile: 1 to save the workspace as a .mat at the end, 0 otherwise
%   **these files can be big for voxelwise analyses**
%   **you may want this if you bootstrap - not all output is amenable to
%   simple pictures or .txt files, but everything is saved in the .mat **
% netmetswitch: 'simple','histograms','smallworld'
%   smallworld requires an additional randomreps argument, which specifies
%   how many times make equivalent random graphs of the real graph
%   **smallworld can also bootstrap your data. if you wish to do this, pass
%   in additional arguments for the # bootstraps to perform, and how many
%   samples to compose each bootstrap from
%
% There's not enough room here to describe each output and what it means.
% All data will be written to a /STEMNAME_PRMS_NETMET_ANALYSISTYPE file.
% Almost all .txt data will have column headers (rows are analyses). Almost
% all .tiffs will have legends and/or axis labels. The measures themselves,
% such as lambda, clustering coefficient, global efficiency, etc, can be
% found quite easily using google.
%
% USAGE: ngt.network_metrics(params,analysistype,xdistance,makebinary,savematfile,net metswitch,*randomreps*,**#bootstraps,bootstrapsamplesize**)
% USAGE: ngt.network_metrics('modbox.prm','thr',[],0,1,'histograms')
% USAGE: ngt.network_metrics('modbox.prm','box',[],1,1,'simple')
% USAGE: ngt.network_metrics('modbox.prm','thr',15,1,1,'smallworld',20)
% USAGE: ngt.network_metrics('modbox.prm','thr',[],1,1,'smallworld',20,1000,30)
%
% NOTE: 'histograms' doesn't completely work on macthunder b/c no curvefit toolbox
%
% AUTHOR: jdp 10/10/10

% read in the prmfile settings
params = ngt.load_prm(params);
if ~exist(params.writepath, 'file')
    mkdir(params.writepath);
end

% create filebase for output
filestem = ngt.filenameprep(params, analysistype);

% determine the # thresholds, etc. for the analysis
[subjectarray thresholdarray numanalyses xarray] = ngt.matrix_parameter_setter(params, analysistype);

% load the matrix
[matrix nodes unused] = ngt.matfile_loader(params.matfile);

% apply a distance exclusion?
if ~isempty(xdistance)
    if isnumeric(xdistance) && (xdistance>=0)
        matrix = ngt.matrix_xdistance(matrix,params.roifile,xdistance);
        xd=ngt.dotremover(num2str(xdistance));
        filestem=[filestem '_xd' xd ];
    else
        error('xdistance is < 0 or not numeric');
    end
end

% check makebinary
switch makebinary
    case 0
    case 1
        filestem=[filestem '_BI' ];
    otherwise
        error('makebinary should be ''0'' or ''1'' ');
end

% check savematfile
switch savematfile
    case 0
    case 1
    otherwise
        error('savematfile should be ''0'' (no .mat saved) or ''1'' (.mat saved) ');
end

switch netmetswitch
    case 'simple'
        
        %%%% SIMPLE - FAST AND GOOD FOR DEFINING ANALYSIS RANGES %%%%
        
        clf;
        
        % create an output directory if needed
        filestem=[params.stemname '_' filestem];
        outdir=[params.writepath '/' filestem '_NETMET_SIMPLE' ];
        if ~exist(outdir, 'file')
            mkdir(outdir);
        end
        
        % initialize output variables
        foundr=zeros(numanalyses,1,'single');
        foundkden=zeros(numanalyses,1,'single');
        N=zeros(numanalyses,1,'single');
        K=zeros(numanalyses,1,'single');
        k=zeros(numanalyses,1,'single');
        kden=zeros(numanalyses,1,'single');
        lnN=zeros(numanalyses,1,'single');
        degrees=zeros(nodes,numanalyses,'single');
        components=zeros(numanalyses,1,'single');
        
        % do analyses at each threshold/boxcar
        for i=1:numanalyses
            fprintf('Thr/box %d\n',i);
            
            % get the appropriate matrix
            rmat = ngt.matrix_former(matrix,subjectarray(i,1),subjectarray(i,2),'2D','diagout');
            
            % threshold the matrix
            [rmat foundr(i,1) foundkden(i,1)] = ngt.matrix_thresholder(rmat,thresholdarray(i,1),params.thresholdtype);
            
            % if user wants binarized networks
            if makebinary
                rmat = single(0 < rmat);
            end
            
            % obtain the fast measures (no complex calculations)
            % nodes edges edgedensity edges/node ln(nodes)
            [N(i,1) K(i,1) kden(i,1) k(i,1) lnN(i,1) degrees(:,i) ] = netmet_basics(rmat);
            
            % obtain the number of components in the network
            [components(i,1)] = componentdetector(rmat);
            
        end
        
        % write out results
        subplot(3,1,1);
        plot(xarray,components,'r.');
        ylabel('components');
        subplot(3,1,2);
        plot(xarray,kden,'r.');
        ylabel('kden');
        subplot(3,1,3);
        plot(xarray,k,'r.',xarray,lnN,'b.');
        ylabel('k & lnN');
        xlabel('Thr/box');
        saveas(gcf,[outdir '/' filestem '.tiff'],'tiff');
        
        fid=fopen([outdir '/' filestem '.txt'],'w');
        fprintf(fid,'Thr/Box\tN\tK\tkden\tk\tlnN\tcomponents\n');
        fclose(fid);
        writemat=[xarray N K kden k lnN components];
        dlmwrite([outdir '/' filestem '.txt'],writemat,'delimiter','\t','-append');
        
        % save the workspace
        if savematfile
            save([outdir '/' filestem '.mat']);
        end
        
    case 'histograms'
        
        %%%% HISTOGRAMS - CDF, DISTANCE VS R PLOTS %%%%
        
        clf;
        
        % create an output directory if needed
        filestem=[params.stemname '_' filestem];
        outdir=[params.writepath '/' filestem '_NETMET_HISTOGRAMS' ];
        if ~exist(outdir, 'file')
            mkdir(outdir);
        end
        
        % get the appropriate matrix
        rmat = ngt.matrix_former(matrix,params.subjectA,params.subjectZ,'2D','diagout');
        
        % if user wants binarized networks
        if makebinary
            rmat = single(0 < rmat);
        end
        
        % get cumulative probability distribution
        cumdist=unique(rmat);
        cdfvals=cdfcalc(cumdist);
        
        % plot and save cdf
        plot(cumdist,cdfvals(2:end,1),'r.');
        xlabel('matrix values');
        ylabel('cumulative probability distribution');
        saveas(gcf,[outdir '/' filestem '_cdf.tiff'],'tiff');
        
        fid=fopen([outdir '/' filestem '_cdf.txt'],'w');
        fprintf(fid,'rvalue\tcumdistribution\n');
        fclose(fid);
        dlmwrite([outdir '/' filestem '_cdf.txt'],[cumdist cdfvals(2:end,1) ],'delimiter','\t','-append');
        
        % write histogram of average matrix
        clf;
        hist(rmat(:),max(20,nnz(rmat)/100));
        xlabel('Correlation value');
        ylabel('# cells with value');
        saveas(gcf,[outdir '/' filestem '_hist.tiff'],'tiff');
        
        % obtain distances between nodes
        [xyz unused] = ngt.roifilereader(params.roifile);
        distmat = ngt.euclidean_distance(params.roifile);
        
        % make masks for appropriate cells
        right=single(xyz(:,1)>=0);
        left=single(xyz(:,1)<0);
        rightmask=logical(right*right');
        leftmask=logical(left*left');
        intermask=logical(right*left');
        
        % plot correlations vs distance, with loess curves
        % what smoothing kernal to use for loess?
        while true
            smoothkernel=input('What size smoothing kernel for the loess curve (suggest: 100) ? ');
            if isnumeric(smoothkernel)
                sk=ngt.dotremover(num2str(smoothkernel));
                filestem=[filestem '_sk' sk ];
                break
            end
        end
        
        subplot(2,2,1);
        [sorteddata loes] = lowess_smoother(distmat(rightmask),rmat(rightmask),smoothkernel);
        plot(distmat(rightmask),rmat(rightmask),'r.',sorteddata(:,1),loes,'k');
        ylim([-.5 1]); xlim([0 180]); xlabel('distance (mm) within R hemi'); ylabel('matrix value');
        subplot(2,2,2);
        [sorteddata loes] = lowess_smoother(distmat(leftmask),rmat(leftmask),smoothkernel);
        plot(distmat(leftmask),rmat(leftmask),'b.',sorteddata(:,1),loes,'k');
        ylim([-.5 1]); xlim([0 180]); xlabel('distance (mm) within L hemi'); ylabel('matrix value');
        subplot(2,2,3);
        [sorteddata loes] = lowess_smoother(distmat(intermask),rmat(intermask),smoothkernel);
        plot(distmat(intermask),rmat(intermask),'g.',sorteddata(:,1),loes,'k');
        ylim([-.5 1]); xlim([0 180]); xlabel('distance (mm) of INTER hemi'); ylabel('matrix value');
        subplot(2,2,4);
        plot(distmat(rightmask),rmat(rightmask),'r.',distmat(leftmask),rmat(leftmask),'b.',distmat(intermask),rmat(intermask),'g.');
        ylim([-.5 1]); xlim([0 180]); xlabel('Distance (mm)'); ylabel('matrix value');
        saveas(gcf,[outdir '/' filestem '_r_vs_distances.tiff'],'tiff');
        
        % print an image of the matrix
        clf;
        imagesc(rmat);
        colorbar;
        saveas(gcf,[outdir '/' filestem '_rmat.tiff'],'tiff');
        
        % save a matfile of these variables
        if savematfile
            save([outdir '/' filestem '.mat']);
        end
        
    case 'smallworld'
        
        %%%% SMALLWORLD - COMPREHENSIVE, SLOWER ANALYSIS %%%%
        %%%% CAN ALSO BOOTSTRAP %%%%
        
        % ensure that binary settings are selected
        if ~makebinary
            error('You have what doctors call a little bit of a weight problem. Choose unweighted analysis for smallworld! -jdp ');
        end
        
        % smallworld needs the number of equivalent random graphs to make
        if isempty(varargin)
            error('must provide value for randomreps');
        end
        if varargin{1,1}>=1
            randomreps=varargin{1,1};
        else
            error('randomreps needs to be >=1.');
        end
        
        
        % set variable dimensions according to bootstrapping needs
        finaldim=1;
        bootstrapping=0;
        if size(varargin,2)>1
            bootstrapping=1;
            finaldim=varargin{1,2};
            if finaldim<1
                error('numbootstraps should be >=1');
            end
            if length(varargin) < 3
                error('must provide bootsamplesize, >= 1');
            end
            bootsamplesize=varargin{1,3};
            if bootsamplesize<1
                error('bootsamplesize should be >=1');
            end
            filestem=[filestem '_boot' num2str(bootsamplesize) 'x' num2str(finaldim) ];
        end
        
        clf;
        
        % initialize variables to zero
        foundr=zeros(numanalyses,finaldim,'single');
        foundkden=foundr;
        N=foundr;
        K=foundr;
        k=foundr;
        kden=foundr;
        lnN=foundr;
        components=foundr;
        latticeCC=foundr;
        latticeL=foundr;
        CC=foundr;
        L=foundr;
        radius=foundr;
        diameter=foundr;
        giantcomponent=foundr;
        graphconnectedness=foundr;
        eglobal=foundr;
        elocal=foundr;
        randomCC=foundr;
        stdrandomCC=foundr;
        randomL=foundr;
        stdrandomL=foundr;
        randomeglobal=foundr;
        stdrandomeglobal=foundr;
        randomelocal=foundr;
        stdrandomelocal=foundr;
        ass=foundr;
        
        degrees=zeros(nodes,numanalyses,finaldim,'single');
        bucc=degrees;
        ecc=degrees;
        enode=degrees;
        
        % create an output directory if needed
        filestem=[params.stemname '_' filestem];
        outdir=[params.writepath '/' filestem '_NETMET_SW' ];
        if ~exist(outdir, 'file')
            mkdir(outdir);
        end
        
        % do analyses at each threshold/boxcar
        for i=1:numanalyses
            
            % this will be 1 normally, or the number of bootstraps
            for j=1:finaldim
                fprintf('Thr/box %d, pass %d\n',i,j);
                
                % get the appropriate matrix
                if bootstrapping
                    rmat = ngt.matrix_former(matrix,subjectarray(i,1),subjectarray(i,2),'2D','diagout',bootsamplesize);
                else
                    rmat = ngt.matrix_former(matrix,subjectarray(i,1),subjectarray(i,2),'2D','diagout');
                end
                
                % threshold the matrix
                [rmat foundr(i,j) foundkden(i,j)] = ngt.matrix_thresholder(rmat,thresholdarray(i,1),params.thresholdtype);
                
                % if user wants binarized networks
                if makebinary
                    rmat = single(0 < rmat);
                end
                
                % obtain the fast measures (no complex calculations)
                % nodes edges edgedensity edges/node ln(nodes)
                [N(i,j) K(i,j) kden(i,j) k(i,j) lnN(i,j) degrees(:,i,j) ] = netmet_basics(rmat);
                
                % obtain the number of components in the network
                components(i,j) = componentdetector(rmat);
                
                % obtain properties of comparable lattices
                [latticeCC(i,j) latticeL(i,j)] = netmet_lattice(N(i,j),k(i,j));
                
                % calculate the clustering coefficient
                [bucc(:,i,j) CC(i,j)] = ngt.bu_clusteringcoefficients(rmat);
                
                % calculate the path lengths (disconnects:reach=0,dist=Inf)
                [L(i,j) ecc(:,i,j) radius(i,j) diameter(i,j) R unused] = ngt.bu_pathlengths(rmat);
                
                % calculate size of biggest component, and
                % graphconnectedness
                giantcomponent(i,j)=max(sum(R))/nodes;
                graphconnectedness(i,j)=(nnz(R))/(N(i,j)*N(i,j));
                
                % calculate global efficiency
                [harmonicdist eglobal(i,j)] = ngt.efficiency(rmat);
                
                % calculate local efficiencies too
                [ enode(:,i,j) elocal(i,j)] = ngt.efficiency(rmat,1);
                
                % calculate properties of random graphs
                [randomCC(i,j) stdrandomCC(i,j) randomL(i,j) stdrandomL(i,j) randomeglobal(i,j) stdrandomeglobal(i,j) randomelocal(i,j) stdrandomelocal(i,j)  ] = ngt.netmet_randomgraphmetrics(rmat,randomreps);
                
                % calculate the assortativity of the graph
                ass(i,j) = ngt.assortativity(rmat);
                
            end
        end
        
        % normalize everything to the lattice
        n_CC=CC./latticeCC;
        n_L=L./latticeL;
        n_randomCC=randomCC./latticeCC;
        n_stdrandomCC=stdrandomCC./latticeCC;
        n_randomL=randomL./latticeL;
        n_stdrandomL=stdrandomL./latticeL;
        n_latticeCC=latticeCC./latticeCC;
        n_latticeL=latticeL./latticeL;
        
        % calculate the SW parameters
        lambda=L./randomL;
        gamma=CC./randomCC;
        sigma=gamma./lambda;
        
        % if we can just make simple output
        if finaldim==1
            % write the main metrics
            fid=fopen([outdir '/' filestem '.txt'],'w');
            fprintf(fid,'Thr/Box\tN\tK\tkden\tk\tlnN\tcomponents\tgiantcomponent\tgraphconnectedness\teglobal\telocal\trandomeglobal\tstdrandomeglobal\trandomelocal\tstdrandomelocal\tCC\tL\tlatticeCC\tlatticeL\trandomCC\tstdrandomCC \trandomL\tstdrandomL\tn_CC\tn_L\tn_latticeCC\tn_latticeL\tn_randomCC\tn_stdrandomCC\tn_randomL\tn_stdrandomL\tlambda\tgamma\tsigma\tassortativity\n');
            fclose(fid);
            writemat=[xarray N K kden k lnN components giantcomponent graphconnectedness eglobal elocal randomeglobal stdrandomeglobal randomelocal stdrandomelocal CC L latticeCC latticeL randomCC stdrandomCC randomL stdrandomL n_CC n_L n_latticeCC n_latticeL n_randomCC n_stdrandomCC n_randomL n_stdrandomL lambda gamma sigma ass];
            dlmwrite([outdir '/' filestem '.txt'],writemat,'delimiter','\t','-append');
            
            % write node clustering coefficient and efficiencies
            dlmwrite([outdir '/' filestem '_localclusteringcoefficients.txt'],bucc,'\t');
            dlmwrite([outdir '/' filestem '_nodeefficiencies.txt'],enode,'\t');
            subplot(1,2,1); imagesc(bucc); xlabel('Thr/box'); ylabel('local clusteringcoefficient'); colorbar;
            subplot(1,2,2); imagesc(enode); xlabel('Thr/box'); ylabel('node efficiency'); colorbar;
            saveas(gcf,[outdir '/' filestem '_localCC_nodeE.tiff'],'tiff');
            close;
            
            % write SW parameters
            subplot(2,2,1); plot(xarray,sigma,'r.'); ylabel('sigma'); xlabel('Thr/box');
            subplot(2,2,2); plot(xarray,lambda,'r.'); ylabel('lambda'); xlabel('Thr/box');
            subplot(2,2,3); plot(xarray,gamma,'r.'); ylabel('gamma'); xlabel('Thr/box');
            subplot(2,2,4); plot(xarray,eglobal,'g.',xarray,randomeglobal,'r.',xarray,elocal,'go',xarray,randomelocal,'ro');
            hold on; errorbar(xarray,randomeglobal,stdrandomeglobal,'r.');
            errorbar(xarray,randomelocal,stdrandomelocal,'r.'); hold off;
            ylabel('global and local (O) efficiency'); xlabel('Thr/box');
            saveas(gcf,[outdir '/' filestem '_SW_parameters.tiff'],'tiff');
            close;
            
            % write SW metrics
            subplot(2,2,1); plot(xarray,CC,'g.',xarray,randomCC,'r.',xarray,latticeCC,'b.'); ylabel('average clustering coefficient'); xlabel('Thr/box');
            hold on; errorbar(xarray,randomCC,stdrandomCC,'r.'); hold off;
            subplot(2,2,2); plot(xarray,L,'g.',xarray,randomL,'r.',xarray,latticeL,'b.'); ylabel('characteristic path length'); xlabel('Thr/box');
            hold on; errorbar(xarray,randomL,stdrandomL,'r.'); hold off;
            subplot(2,2,3); plot(xarray,n_CC,'g.',xarray,n_randomCC,'r.',xarray,n_latticeCC,'b.'); ylabel('normalized CC'); xlabel('Thr/box');
            hold on; errorbar(xarray,n_randomCC,n_stdrandomCC,'r.'); hold off;
            subplot(2,2,4); plot(xarray,n_L,'g.',xarray,n_randomL,'r.',xarray,n_latticeL,'b.'); ylabel('normalized L'); xlabel('Thr/box');
            hold on; errorbar(xarray,n_randomL,n_stdrandomL,'r.'); hold off;
            saveas(gcf,[outdir '/' filestem '_SW_metrics.tiff'],'tiff');
            close;
            
            % write density/component info
            subplot(2,2,1); plot(xarray,components,'r.'); ylabel(' # components');
            subplot(2,2,2); plot(xarray,giantcomponent,'r.'); ylabel('% nodes in giant component');
            subplot(2,2,3); plot(xarray,kden,'r.',xarray,graphconnectedness,'b.'); ylabel('kden and graph connectedness'); xlabel('Thr/box');
            subplot(2,2,4); plot(xarray,k,'r.',xarray,lnN,'b.'); ylabel('k & lnN'); xlabel('Thr/box');
            saveas(gcf,[outdir '/' filestem '_components_and_density.tiff'],'tiff');
            close;
            
            % write the assortativity
            plot(xarray,ass,'r.'); xlabel('Thr/box'); ylabel('Assortativity');
            saveas(gcf,[outdir '/' filestem '_assortativity.tiff'],'tiff');
            
        else % if output is complicated by bootstrapping
            
            % write SW parameters
            subplot(2,2,1); plot(xarray,mean(sigma,2),'r.'); ylabel('sigma'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(sigma,2),std(sigma,[],2),'r.'); hold off;
            subplot(2,2,2); plot(xarray,mean(lambda,2),'r.'); ylabel('lambda'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(lambda,2),std(lambda,[],2),'r.'); hold off;
            subplot(2,2,3); plot(xarray,mean(gamma,2),'r.'); ylabel('gamma'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(gamma,2),std(gamma,[],2),'r.'); hold off;
            subplot(2,2,4); plot(xarray,mean(eglobal,2),'g.',xarray,mean(randomeglobal,2),'r.',xarray,mean(elocal,2),'go',xarray,mean(randomelocal,2),'ro');
            hold on; errorbar(xarray,mean(eglobal,2),std(eglobal,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(randomeglobal,2),std(randomeglobal,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(elocal,2),std(elocal,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(randomelocal,2),std(randomelocal,[],2),'r.'); hold off;
            ylabel('global and local (O) efficiency'); xlabel('Thr/box');
            saveas(gcf,[outdir '/' filestem '_SW_parameters.tiff'],'tiff');
            close;
            
            % write SW metrics
            subplot(2,2,1); plot(xarray,mean(CC,2),'g.',xarray,mean(randomCC,2),'r.',xarray,mean(latticeCC,2),'b.'); ylabel('average clustering coefficient'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(CC,2),std(CC,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(randomCC,2),std(randomCC,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(latticeCC,2),std(latticeCC,[],2),'b.'); hold off;
            subplot(2,2,2); plot(xarray,mean(L,2),'g.',xarray,mean(randomL,2),'r.',xarray,mean(latticeL,2),'b.'); ylabel('characteristic path length'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(L,2),std(L,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(randomL,2),std(randomL,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(latticeL,2),std(latticeL,[],2),'b.'); hold off;
            subplot(2,2,3); plot(xarray,mean(n_CC,2),'g.',xarray,mean(n_randomCC,2),'r.',xarray,mean(n_latticeCC,2),'b.'); ylabel('normalized CC'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(n_CC,2),std(n_CC,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(n_randomCC,2),std(n_randomCC,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(n_latticeCC,2),std(n_latticeCC,[],2),'b.'); hold off;
            subplot(2,2,4); plot(xarray,mean(n_L,2),'g.',xarray,mean(n_randomL,2),'r.',xarray,mean(n_latticeL,2),'b.'); ylabel('normalized L'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(n_L,2),std(n_L,[],2),'g.'); hold off;
            hold on; errorbar(xarray,mean(n_randomL,2),std(n_randomL,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(n_latticeL,2),std(n_latticeL,[],2),'b.'); hold off;
            saveas(gcf,[outdir '/' filestem '_SW_metrics.tiff'],'tiff');
            close;
            
            % write density/component info
            subplot(2,2,1); plot(xarray,mean(components,2),'r.'); ylabel(' # components');
            hold on; errorbar(xarray,mean(components,2),std(components,[],2),'r.'); hold off;
            subplot(2,2,2); plot(xarray,mean(giantcomponent,2),'r.'); ylabel('% nodes in giant component');
            hold on; errorbar(xarray,mean(giantcomponent,2),std(giantcomponent,[],2),'r.'); hold off;
            subplot(2,2,3); plot(xarray,mean(kden,2),'r.',xarray,mean(graphconnectedness,2),'b.'); ylabel('kden and graph connectedness'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(kden,2),std(kden,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(graphconnectedness,2),std(graphconnectedness,[],2),'b.'); hold off;
            subplot(2,2,4); plot(xarray,mean(k,2),'r.',xarray,mean(lnN,2),'b.'); ylabel('k & lnN'); xlabel('Thr/box');
            hold on; errorbar(xarray,mean(k,2),std(k,[],2),'r.'); hold off;
            hold on; errorbar(xarray,mean(lnN,2),std(lnN,[],2),'b.'); hold off;
            saveas(gcf,[outdir '/' filestem '_components_and_density.tiff'],'tiff');
            close;
            
            % write the assortativity
            plot(xarray,mean(ass,2),'r.'); xlabel('Thr/box'); ylabel('Assortativity');
            hold on; errorbar(xarray,mean(ass,2),std(ass,[],2),'r.'); hold off;
            saveas(gcf,[outdir '/' filestem '_assortativity.tiff'],'tiff');
            close;
            
            % write these outputs to a txt file
            fid=fopen([outdir '/' filestem '.txt'],'w');
            fprintf(fid,'Thr/Box\tN\tK\tstdK\tk\tstdk\tlnN\tcomponents\tstdcomponents\tgiantcomponents\tstdgiantcomponents\tgraphconnectedness\tstdgraphconnectedness\teglobal\tstdeglobal\telocal\tstdelocal\trandomeglobal\tstdrandomeglobal\trandomelocal\tstdrandomelocal\tCC\tstdCC\tL\tstdL\tlatticeCC\tstdlatticeCC\tlatticeL\tstdlatticeL\trandomCC\tstdrandomCC\trandomL\tstdrandomL\tn_CC\tstdn_CC\tn_L\tstdn_L\tn_latticeCC\tstdn_latticeCC\tn_latticeL\tstdn_latticeL\tn_randomCC\tstdn_randomCC\tn_randomL\tstdn_randomL\tlambda\tstdlambda\tgamma\tstdgamma\tsigma\tstdsigma\tass\tstdass\n');
            fclose(fid);
            writemat=[xarray mean(N,2) mean(K,2) std(K,[],2) mean(k,2) std(k,[],2) mean(lnN,2) mean(components,2) std(components,[],2) mean(giantcomponent,2) std(giantcomponent,[],2) mean(graphconnectedness,2) std(graphconnectedness,[],2) mean(eglobal,2) std(eglobal,[],2) mean(elocal,2) std(elocal,[],2) mean(randomeglobal,2) std(randomeglobal,[],2) mean(randomelocal,2) std(randomelocal,[],2) mean(CC,2) std(CC,[],2) mean(L,2) std(L,[],2) mean(latticeCC,2) std(latticeCC,[],2) mean(latticeL,2) std(latticeL,[],2) mean(randomCC,2) std(randomCC,[],2) mean(randomL,2) std(randomL,[],2) mean(n_CC,2) std(n_CC,[],2) mean(n_L,2) std(n_L,[],2) mean(n_latticeCC,2) std(n_latticeCC,[],2) mean(n_latticeL,2) std(n_latticeL,[],2) mean(n_randomCC,2) std(n_randomCC,[],2) mean(n_randomL,2) std(n_randomL,[],2) mean(lambda,2) std(lambda,[],2) mean(gamma,2) std(gamma,[],2) mean(sigma,2) std(sigma,[],2) mean(ass,2) std(ass,[],2) ];
            dlmwrite([outdir '/' filestem '.txt'],writemat,'delimiter','\t','-append');
            
        end
        
        % save a matfile of these variables
        if savematfile
            save([outdir '/' filestem '.mat']);
        end
        
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [N K kden k lnN degrees] = netmet_basics(mat)

% this calculates quick properties of a network

d=size(mat);
N=d(1);
K=nnz(triu(mat,1));
kden=K/(N*(N-1)/2);
k=(K*2)/N;
lnN=log(N);
degrees=sum(mat);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function components = componentdetector(mat)

% this almost always identifies the right number of components in a network

nodes=size(mat,1);

mains=-sum(mat);
mains=diag(mains);
mat=mat.*(~eye(nodes));
mat=mat+mains;

[V D] = eig(mat);
D=diag(D);
components=nnz(abs(D)<0.0001);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sorteddata loes] = lowess_smoother(xvals,yvals,smoothkernel)

data(:,1)=xvals; data(:,2)=yvals;
sorteddata=sortrows(data);
loes=smooth(sorteddata(:,2),smoothkernel,'lowess');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [latticeCC latticeL] = netmet_lattice(N,k)

latticeCC = (3*k-3)/(4*k-2);
latticeL = N/k;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

