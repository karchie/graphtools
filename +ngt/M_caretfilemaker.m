function M_caretfilemaker(name,xyz,rgb,shape,fociname,focicolorname,varargin)
% M_CARETFILEMAKER Map ROI onto caret surfaces using .foci files
%
% This file maps ROIs onto caret surfaces using .foci files. It accepts a
% list of names and coordinates, with associated shapes and colors, and
% writes those to .foci and .focicolor files for use in Caret.
%
% USAGE: ngt.M_caretfilemaker(roiname,roixyz,roirgb,roishape,fociname,focicolorname,*alpha,pointsize,linesize*)
% USAGE: ngt.M_caretfilemaker(roiname,roixyz,roirgb,roishape,fociname,focicolorname)
%
% NOTE: alpha=255, pointsize=2.5, and linesize=1 by default
%
% AUTHOR jdp 10/10/10

alpha=255;
pointsize=2.5;
linesize=1;
if ~isempty(varargin)
    if length(varargin) ~= 3
        error('must specify alpha, pointsize, and linesize')
    end
    alpha=varargin{1,1};
    pointsize=varargin{1,2};
    linesize=varargin{1,3};
end

%%%%% FOCI FILE %%%%%

% write a .foci file header
fid=fopen(fociname, 'wt');
fprintf(fid,'CSVF-FILE,0,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'csvf-section-start,header,3,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'tag,value,,,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'caret-version,5.51,,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'date,Tue Oct 2 15:58:38 2007,,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'encoding,COMMA_SEPARATED_VALUE_FILE,,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'csvf-section-end,header,,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'csvf-section-start,Cells,15,,,,,,,,,,,,,,,,,\n');
fprintf(fid,'Cell Number,X,Y,Z,Section,Name,Study Number,Geography,Area,Size,Statistic,Comment,Structure,Class Name,Study Pubmed ID,Study Table Number,Study Table Subheader,Study Figure Number,Study Figure Panel,Study page Number\n');

% write the contents of the .foci file (where the ROIs are)
for i=1:size(xyz,1)
    side='right';
    if xyz(i,1) < 0
        side='left';
    end
    fprintf(fid,'%d,%d,%d,%d,0,%s,1,,,10,,,%s,,0\n',i,xyz(i,1),xyz(i,2),xyz(i,3),name{i,1},side);
end

% close the .foci file
fprintf(fid,'csvf-section-end,Cells,,,,,,,,,,,,,');
fclose(fid);

%%%%% FOCICOLOR FILE %%%%%

% now write a .focicolor header
fid=fopen(focicolorname, 'wt');
fprintf(fid,'CSVF-FILE,0,,,,,,,,\n');
fprintf(fid,'csvf-section-start,header,3,,,,,,,\n');
fprintf(fid,'tag,value,,,,,,,,\n');
fprintf(fid,'caret-version,5.51,,,,,,,,\n');
fprintf(fid,'comment,[Enter comment],,,,,,,,\n');
fprintf(fid,'date,[Enter date],,,,,,,,\n');
fprintf(fid,'encoding,COMMA_SEPARATED_VALUE_FILE,,,,,,,,\n');
fprintf(fid,'pubmed_id,,,,,,,,,\n');
fprintf(fid,'csvf-section-end,header,,,,,,,,\n');
fprintf(fid,'csvf-section-start,Colors,9,,,,,,,\n');
fprintf(fid,'Name,Red,Green,Blue,Alpha,Point-Size,Line-Size,Symbol,,\n');

% and the contents of the .focicolor file
for i=1:size(xyz,1)
    fprintf(fid,'%s,%d,%d,%d,%d,%d,%d,%s,,\n',name{i},rgb(i,1),rgb(i,2),rgb(i,3),alpha,pointsize,linesize,shape{i,1});
end

% close the .focicolor file
fprintf(fid,'csvf-section-end,Colors,,,,,,,,');
fclose(fid);
