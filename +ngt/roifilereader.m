function [xyz nodenames] = roifilereader(roifile)
% ROIFILEREADER Read coordinates and node names from an roifile.
%    [xyz nodenames] = ngt.roifilereader(roifile) returns the xyz coordinates
%    and node names from the given ROI file.
%
% Author: jdp 10/10/10
fid=fopen(roifile,'r');
C=textscan(fid,'%d%d%d%d%s%s%s%s%s','HeaderLines',1);
fclose(fid);
xyz = [C{1,2} C{1,3} C{1,4}];
nodenames=C{1,5};