function string2 = dotremover(string1)
% DOTREMOVER Removes dots (.) from a string.
%    s1 = ngt.dotremover(s0) produces a copy of s0 with the dots removed.
%
% AUTHOR: jdp 9/15/10
string2=regexprep(string1,'\.','');