function filestem = filenameprep(params, analysistype)
% FILESTEM Prepares a filestem from the provided parameters
% Author: Jonathan Power (10/10/10)
% Adapted to parameters structure by Kevin A. Archie <karchie@wustl.edu>
params = ngt.load_prm(params);

switch params.thresholdtype
    case 'r'
        ttype='r';
    case 'kden'
        ttype='k';
    case 'abs'
        ttype='abs';
    otherwise
        error('thresholdtype must be ''r'' ''kden'' or ''abs''');
end

switch analysistype
    case 'thr'
        threshpart = sprintf('T%s%gto%gin%g', ttype, params.loend, params.hiend, params.step);
        subjectpart = sprintf('S%gto%g', params.subjectA, params.subjectZ);
    case 'box'
        threshpart = sprintf('T%s%g', ttype, params.threshold);
        subjectpart = sprintf('S%gto%g_BX%gst%gs', params.subjectA, params.subjectZ, params.boxcarsize, params.boxcarstep);
    case 'neither'
        error('analysistype must be ''thr'' or ''box''');
end

filestem=ngt.dotremover([threshpart '_' subjectpart]);
