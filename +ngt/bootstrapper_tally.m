function tallymat = bootstrapper_tally(clrcolumn)
% BOOTSTRAPPER_TALLY Creates binary node
%
% This takes a column of node assignments and creates a binary matrix of
% node x node, showing whether nodes were in the same community (1) or not
% (0).
% 
% USAGE: tallymat = ngt.bootstrapper_tally(clrcolumn)
%
% AUTHOR: jdp 10/10/10

clrcolumn=single(clrcolumn);
nodes=size(clrcolumn,1);
mods=unique(clrcolumn);
nummods=nnz(mods);
tallymat=zeros(nodes,nodes,'single');
for m=1:nummods
    thesepairs=single(clrcolumn==mods(m));
    tallymat=tallymat+(thesepairs*thesepairs');
end