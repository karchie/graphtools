function [normmat] = matrix_normalize(mat)

normmat = mat./(repmat(max(mat),[size(mat,1) 1]));