function [mastertally proportiontally] = pairtallyer(clrfile,varargin)
% PAIRTALLYER Tally shared assignments between nodes
%
% This script accepts a textfile or matrix of community assignments (a
% clrfile typically), and tallies the number of shared assignments between
% nodes. A first and last column to consider can also be passed in. The
% mastertally (absolute count) and the proportiontally (absolute count
% divided by the number of columns considered) are returned as variables
%
% clrfile: a matrix or .txt of clr assignments
% colA: the first column of the matrix to consider
% colZ: the final column of the matrix to consider
% mastertally: of N columns, how often were node I and J in a community together
% proportiontally: mastertally/#columns
%
% USAGE: [mastertally proportiontally] = ngt.pairtallyer(clrfile,*colA,colZ*)
% USAGE: [mastertally proportiontally] = ngt.pairtallyer('clrs.txt')
% USAGE: [mastertally proportiontally] = ngt.pairtallyer(colormatrix)
% USAGE: [mastertally proportiontally] = ngt.pairtallyer('clrs.txt',4,8)
%
% AUTHOR: jdp 10/10/10

% load the clrs if needed; ensure clrfile is defined for output
if ~isnumeric(clrfile)
    clrs=load(clrfile);
else
    clrs=clrfile;
    clrfile='matrix';
end

% only the portion of interest, if user desires
if ~isempty(varargin)
    if length(varargin) < 2
        error('must provide colA and colZ');
    end
    colA=varargin{1,1};
    colZ=varargin{1,2};
    clrs=clrs(:,colA:colZ);
end

% tally over every column of interest
[a b]=size(clrs);
mastertally=zeros(a,a);
for i=1:b
    
    % load up the columns 1 by 1
    clear thiscolumn;
    thiscolumn=clrs(:,i);
    
    % find the pairs in the same groups
    clear pairtally;
    pairtally=ngt.bootstrapper_tally(thiscolumn);
    
    % track the number of times nodes were paired
    mastertally=mastertally+pairtally;
    
end

% calculate the proportion of shared assignments
proportiontally=mastertally/b;

% save mastertally and proportiontally
[pth fname unused]=ngt.filenamefinder(clrfile,'dotsout');
if ~isempty(varargin)
    base = sprintf('%s%gto%g', fname, colZ, colZ);
    save([base '_tally.mat'], mastertally);
    save([base '_tally_proportion.mat'], proportiontally);
else
    save([clrfile '_tally.mat' ],'mastertally');
    save([clrfile '_tally_proportion.mat' ],'proportiontally');
end
