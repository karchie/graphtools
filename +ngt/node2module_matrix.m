function [ MDCX nMDCX nodesize modulename modulenumber firstexamplex firstexampley] = node2module_matrix(colormat,mat)
% NODE2MODULE_MATRIX
[a b]=size(colormat);
[c d e]=size(mat);

if ~(isequal(a,c,d) && isequal(b,e))
    fprintf('Colors and matrix depth don''t match.\n');
else
    colors=unique(colormat);
    numcolors=size(colors,1);
    MDCX=zeros(numcolors,numcolors,e);
    nMDCX=zeros(numcolors,numcolors,e);
    nodesize=zeros(numcolors,e);
    modulename=cell(numcolors,e);
    
    for x=1:numcolors
        [firstexamplex(x,1) firstexampley(x,1)] =find(colormat==colors(x),1);
    end
    
    for i=1:e
        clear thismat; clear thisframe
        thismat=mat(:,:,i);
        thisframe=colormat(:,i);
        for x=1:numcolors
            nodesize(x,i)=nnz(thisframe==colors(x));
            modulename{x,i} = [ 'M' num2str(colors(x)) ];
            modulenumber(x,i)=colors(x);
            
            for y=1:numcolors
                
                % calculate strengths of connections between modules
                MDCX(x,y,i)=sum(sum(thismat(thisframe==colors(x),thisframe==colors(y))));
                
                % calculate possible number of connections between modules
                clear ynum;
                ynum=nnz(thisframe==colors(y));
                if x==y
                    norm(x,y,i)=nodesize(x,i)*(ynum-1);
                else
                    norm(x,y,i)=nodesize(x,i)*ynum;
                end
                
                % normalize MDCX to possible connections
                if MDCX(x,y,i)~=0
                    nMDCX(x,y,i)=MDCX(x,y,i)/norm(x,y,i);
                else
                    nMDCX(x,y,i)=0;
                end
            end
        end
    end
end