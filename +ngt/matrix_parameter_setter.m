function [subjectarray thresholdarray numanalyses xarray] = ...
    matrix_parameter_setter(params,analysistype)
% MATRIX_PARAMETER_SETTER Generates parameter arrays for graphtool
% analysis.
%
%   [subjects thresholds n xs] = matrix_parameter_setter(params, type)
%    given a parameters structure params and analysis type 'thr' or 'box'
%    returns:
%    n         : number of analyses to be performed
%    xs        : threshold/subjectA of the thr/box setting for analyses
%    subjects  : [subjectA:subjectZ] array of subjects for analyses
%    thresholds: array of thresholds for analyses
%
% Author: jdp 10/10/10
params = ngt.load_prm(params);
switch analysistype
    case 'thr'
        thresholdarray=(params.loend:params.step:params.hiend)';
        d=size(thresholdarray);
        subjectarray=repmat([params.subjectA params.subjectZ],[d(1) 1]);
        xarray=thresholdarray;
    case 'box'
        begins=(params.subjectA:params.boxcarstep:params.subjectZ)';
        ends=begins+params.boxcarsize;
        if (ends(1,1)>params.subjectZ)
            error('These boxcar settings are impossible.');
        end
        while (ends(end,1) > params.subjectZ)
            begins(end,:)=[];
            ends(end,:)=[];
        end
        subjectarray=[begins ends];
        d=size(subjectarray);
        thresholdarray=repmat(params.threshold,[d(1) 1]);
        xarray=begins;
    otherwise
        error('matrix_parameter_setter:badAnalysisType',...
            'Use ''thr'' or ''box'' for analysis types');
end

numanalyses=d(1);
