function Ci = infomap_wrapper(roifile,rmat,pajekfilename,reps,deleteit)
% INFOMAP_WRAPPER Runs infomap on a Pajek file produced from an ROI and
% matrix
%
% This script takes an roifile and a matrix, a name to write a pajekfile
% to, and then runs infomap on the pajekfile with some number of
% repetitions. It then does or does not delete the output, depending on
% deleteit status, and it returns the community assignments found.
%
% USAGE: modules = ngt.infomap_wrapper(roifile,rmat,pajekfilename,reps,deleteit)
%
% AUTHOR: jdp 10/10/10

% create the pajekfile
ngt.mat2pajek(rmat,roifile,pajekfilename);

% this will be the relevant output of infomap
[pathstr,cluname,unused] = ngt.filenamefinder(pajekfilename,'dotsout');
clufile = [ pathstr '/' cluname '.clu' ];

% obtain seed #
clear randnum;
randnum=ceil(rand*1000000);

ngt.siteconfig
if infomap_use_machine_suffix
    % append a machine suffix to the infomap executable name
    command='uname -m';
    [unused systemversion]=system(command);
    systemversion(end)=[]; % remove that carriage return
    infomap_command = [infomap_command '_' systemversion];
end
[status,unused] = system(['which ' infomap_command]);
if status
    error(['infomap executable ' infomap_command ' not found'])
end

% run infomap
c=clock;
fprintf('\t%2.0f:%2.0f:%2.0f: infomap beginning\n',c(4),c(5),c(6));
command = sprintf('%s %g %s %g >junk.txt', infomap_command, randnum, pajekfilename, reps);
[status,response] = system(command);
if status
    error(['infomap command ' infomap_command ' failed: ' response]);
end
c=clock;
fprintf('\t%2.0f:%2.0f:%2.0f: infomap finished\n',c(4),c(5),c(6));

% suppress and remove screen output
command = 'rm junk.txt';
system(command);

% get the column of assignments
fid=fopen(clufile,'r');
tsc=textscan(fid,'%d','HeaderLines',1);
Ci=double(tsc{1});

if deleteit
    command = [ 'rm ' pathstr '/' cluname '*' ];
    system(command);
end

