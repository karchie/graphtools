function distmat = euclidean_distance(xyz)
% EUCLIDEAN_DISTANCE Compute Euclidean distance between ROIs
%
% The rois coordinates can be passed in directly, or an roifile can be passed in
%
% USAGE: distmat = ngt.euclidean_distance(roifile)
% USAGE: distmat = ngt.euclidean_distance('roifile.roi')
% USAGE: distmat = ngt.euclidean_distance(xyz)
% 
% AUTHOR: jdp 10/10/10

if ~isnumeric(xyz)
    % load the roifile
    [xyz unused] = ngt.roifilereader(xyz);
end

xyz=double(xyz);

% calculate the distances between ROIs
distmat=single(squareform(pdist(xyz)));

