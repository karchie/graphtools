function matrix = matrix_xdistance(matrix,roifile,xdistance)
% MATRIX_XDISTANCE Exclude edges shorter than the specified distance
%
% This script takes a matrix, an roifile (or xyz array) and an exclusion
% distance, calculates the distances between the rois, and applies a mask
% to exclude edges closer than an exclusion distance from the matrix. The
% matrix is then returned.
%
% USAGE: matrix = ngt.matrix_xdistance(matrix,roifile,xdistance)
%
% AUTHOR: jdp 10/10/10

% find euclidean distances between ROIs
dmat = ngt.euclidean_distance(roifile);

% make an exclusion mask
dmat=dmat<xdistance;

% apply the exclusion mask to each subject
for i=1:size(matrix,3)
    tempmat=matrix(:,:,i);
    tempmat(dmat)=0;
    matrix(:,:,i)=tempmat;
end
