function [E Eavg] = efficiency(G, unused)
% EFFICIENCY Calculate the efficiency of a network.
%
% Adapted from the BCT efficiency function by Mika Rubinov.
%
% Efficiency is the harmonic mean of pathlengths in a network. For an
% entire network, the mean of pathlength inverses (ignoring the diagonal)
% is the global efficiency. For a local network defined by the neighbors of
% a node (the source node not included), the efficiency defines the source
% node efficiency. The average node efficiency is the local efficiency.
%
% This is all proposed in Latora and Marchiori, 2001 PRL
%
% Ifi you just enter a matrix, it returns the inverse path lengths and the
% global efficiency, but if you give a matrix and a 1 (as a second
% argument), it returns the node efficiencies and the local efficiency.
%
% USAGE: [invpaths eglobal]=ngt.efficiency(matrix)
% USAGE: [nodeefficiencies elocal]=ngt.efficiency(matrix,1)
%
% NOTE: This is currently vetted only for binary, undirected graphs!
%
% AUTHOR: jdp 10/10/10

G=G.*(~eye(size(G,1),size(G,1)));

if nargin==2;                           %local efficiency
    N=length(G);                        %number of nodes
    E=zeros(N,1);                       %local efficiency
    
    for u=1:N
        V=find(G(u,:));                 %neighbors
        k=length(V);                    %degree
        if k>=2;                        %degree must be at least two
            e=distance_inv(G(V,V));
            E(u)=sum(e(:))./(k.^2-k);	%local efficiency
        end
    end
    Eavg=mean(E);
else
    E=distance_inv(G);                  %global efficiency
    diagoutmask=~eye(size(E,1));
    Eavg=mean(E(diagoutmask));
end

function D=distance_inv(g)
D=eye(length(g));
n=1;
nPATH=g;                        %n-path matrix
L=(nPATH~=0);                   %shortest n-path matrix

while find(L,1);
    D=D+n.*L;
    n=n+1;
    nPATH=nPATH*g;
    L=(nPATH~=0).*(D==0);
end

D(~D)=inf;                      %disconnected nodes are assigned d=inf;
D=1./D;                         %invert distance
D=D-eye(length(g));