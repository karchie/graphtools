function quickroifile(numrois,varargin)
% QUICKROIFILE Generate an ROI file
%
% This is a versatile script that makes roifiles. It works in several ways,
% illustrated below:
%
% The roifile is modeled after the standard SoNIA roifile (the header is
% ignored):
%
% ID X Y Z NAME CATA COLORA CATB COLORB
% 1 3 7 6 PCC Brain Black Brain Black
% 2 6 2 2 ACC Brain Black Brain Black
% 3 9 1 3 pFC Brain Black Brain Black
% ...
%
% USAGE: ngt.quickroifile(numrois/roifilename/roiarray,filename)
% USAGE: ngt.quickroifile(23) - makes 23.roi, an roifile with 23 dummy positions
% USAGE: ngt.quickroifile(23,'mine') - makes mine.roi, an roifile with 23 dummy positions
% USAGE: ngt.quickroifile('xyzlist.txt') - makes xyzlist.roi, with rois corresponding to the coordinates
% USAGE: ngt.quickroifile('xyzlist.txt','newroifile') - makes newroifile.roi, with rois corresponding to the coordinates
% USAGE: ngt.quickroifile(xyz) - makes 44.roi, for an xyz array of 44 coordinates
% USAGE: ngt.quickroifile(xyz,'mynewrois') - makes mynewrois.roi, with rois cooresponding to the xyz coordinates
%
% NOTES:
% 10/13/10 updated the header for graphtools
%
% Author: jdp 10/10/10

if isnumeric(numrois)
    if size(numrois,2)==3
        xyz=numrois;
    else
        xyz=1:numrois;
        xyz=xyz';
        xyz=repmat(xyz,[1 3]);
    end
    filename = sprintf('%s/%g.roi', pwd, size(xyz,1));
elseif exist(numrois, 'file')==2 % if you have an xyz list you want make into an roifile
    xyz=load(numrois);
    [pth fname unused]=ngt.filenamefinder(numrois,'dotsout');
    filename=[ pth '/' fname '.roi' ];
end


if ~isempty(varargin)
    filename=varargin{1,1};
end


fid=fopen(filename,'w');
fprintf(fid,'ROI\tX\tY\tZ\tName\tAnatomy\tAcolor\tBname\tBcolor\n');
for i=1:size(xyz,1);
    tempname = sprintf('%g_%g_%g', xyz(i,1), xyz(i,2), xyz(i,3));
    fprintf(fid,'%d\t%d\t%d\t%d\t%s\tBrain\tBlack\tBrain\tBlack\n',i,xyz(i,1),xyz(i,2),xyz(i,3),tempname);
end
fclose(fid);


