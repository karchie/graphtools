function [lambda ecc radius diameter R D] = bu_pathlengths(mat)
% BU_PATHLENGTHS Compute reachability and minimum path lengths for binary,
% undirected networks.
%
% This script consolidates 2 scripts from BCT, which calculate
% reachability and minimum path lengths for binary, undirected
% matrices. These values are then turned into average path length, as well
% as eccentricity and radius/diameter measures of the network. The
% calculations require binary matrices with zeroed diagonals, which is
% enforced at the beginning of the script.
%
% NOTE: I ignore both Inf and diagonal (self-connecting paths) when
% calculating shortest path measures!!! D and R include them, but they are
% ignored for lambda and eccentricity calculations!
%
% USAGE: [lambda ecc radius diameter R D] = ngt.bu_pathlengths(mat)
%
% AUTHOR: jdp 10/10/10

% remove the diagonal if necessary
a=diag(mat);
if nnz(a)>0
    mat=mat.*(~eye(size(mat,1)));
end

% binarize the matrix
mat = single(0 < mat);

% find pathlengths and define the reachability matrix
D=single(internal_bu_pathlengths(mat));
R=single(D~=Inf);

% D reflects all shortest paths, and we wish to ignore self-connecting paths (the diagonal) and Infs
b=triu(D,1);
numtruepaths=nnz((b(b~=Inf)));
sumtruepaths=sum((b(b~=Inf)));

% average shortest path length
lambda=sumtruepaths/numtruepaths;

% now the maximal shortest path for each node
ecc = max(D.*(D~=Inf),[],2);
radius = min(ecc);
diameter = max(ecc);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function D=internal_bu_pathlengths(mat)

% jdp 10/10/10
%
% This script was in Mika's efficiency script. It is orders of magnitude
% faster than our previous shortest path lengths script (from Olaf too).
% The idea is that an adjacency matrix to the Nth power shows cells where a
% path between nodes I and J takes N steps. This is computationally
% blazing. I've commented Mika's script for posterity.
%
% USAGE: distances = ngt.bu_pathlengths(matrix)

% initialize the distance matrix
D=zeros(size(mat,1));

% n will be the power the matrix is raised to
n=1;

% paths of length one are identical to the adjacency matrix
nPATH=mat;

% L will keep track of where new shortest paths appear
L=(nPATH~=0);

% while at least one shortest path was just added (in the previous/first
% cycle)
while find(L,1);
    
    % enter the new L values in the distance matrix
    D=D+n.*L;
    
    % move the power up one
    n=n+1;
    
    % raise the nPATH matrix to the next power (g^n)
    nPATH=nPATH*mat;
    
    % now show new additions (D must be empty, and nPATH has an entry)
    L=(nPATH~=0).*(D==0);
end

% entries that are empty get an Inf to indicate disconnection
D(~D)=inf;

