function [mCC stdmCC mL stdmL eglobal stdeglobal elocal stdelocal ] = netmet_randomgraphmetrics(matrix,reps)
% NETMET_RANDOMGRAPHMETRICS Compute statistics on randomized matrices
%
% This script takes a network, and makes a number of random versions of it.
% For each of these randomized versions, efficiency and small-world
% calculations are performed, and the means and stds of these values are
% returned by the script.
%
% NOTE: Randomizations can take REALLY long (or infinite!) times if you
% haven't chosen your thresholds well. Very dense or ridiculously sparse
% matrices will encounter such problems, so be smart about thresholding.
%
% USAGE: [meanCC stdCC meanL stdL meaneglobal stdeglobal meanelocal stdelocal ] = ngt.netmet_randomgraphmetrics(matrix,randomrepetitions)
%
% AUTHOR: jdp 10/10/10
for i=1:reps
    
    % obtain a randomized matrix (100% rewiring, 1 cycle rewiring)
    randmat = ngt.randmio_und(matrix,1,1);
    
    % calculate small world stuff (CC and L)
    [bucc(:,i) randCC(i,1)]=ngt.bu_clusteringcoefficients(randmat);
    [randL(i) ecc(:,i) radius(i,1) diameter(i,1) unused unused] = ngt.bu_pathlengths(randmat);
    
    % calculate efficiencies
    [ invharm tempeglobal(i,1) ] = ngt.efficiency(randmat);
    [ tempenode(:,i) tempelocal(i,1)] = ngt.efficiency(randmat,1);
    
end

eglobal=mean(tempeglobal(:));
stdeglobal=std(tempeglobal(:));
elocal=mean(tempelocal(:));
stdelocal=std(tempelocal(:));

mCC=mean(randCC);
stdmCC=std(randCC);
mL=mean(randL);
stdmL=std(randL);
