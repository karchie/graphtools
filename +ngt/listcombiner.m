function allrois = listcombiner(type,excludedistance,outputname,varargin)
% LISTCOMBINER Remove overlaps between xyzfiles to form a combined list
%
% This removes identical overlaps between xyzfiles, and then eliminates
% overlaps between the lists to form a final combined list, which is
% written to a file, and returned as the allrois variable.
%
% type: 'equal' or 'ranked'
% excludeddistance: exclude rois within X Euclidean mm of each other
% outputname: what to write output to
% varargin: the xyz files
%
% USAGE: allrois = ngt.listcombiner(type,excludedistance,outputname,varargin)
% USAGE: allrois = ngt.listcombiner('ranked',10,'filtered','xyzlist1.txt')
% USAGE: allrois = ngt.listcombiner('equal',15,'newrois','xyzlist1.txt','xyzlist2.txt')
%
% AUTHOR: jdp 8/10/10

% how many files to combine?
numfiles=size(varargin,2);

% concatenate the files into a single list
for i=1:numfiles
    % load either the matrix or the file
    clear a;
    if isnumeric(varargin{1,i})
        a=varargin{1,i};
    else
        a=load(varargin{1,i});
    end
    
    % concatenate the newest rois into allrois
    if i==1
        allrois=a;
    else
        allrois=[allrois;a];
    end
end

% find the initial distances and bad actors
startnumrois=size(allrois,1);
distmat=squareform(pdist(allrois));
badones=(distmat<excludedistance);
[badguys(:,1) badguys(:,2)]=find(triu(badones,1));
badguys=sortrows(badguys,2);


% if there are no overlaps to be found
if isempty(badguys)
    fprintf('No overlaps found\n');
else
    % but if there are, loop through discarding a random overlapper or the
    % highest node added until no overlaps remain
    while ~isempty(badguys)
        dbad=size(badguys);
        
        % discard random node or last-added node from overlaps
        switch type
            case 'equal'
                randbad=ceil(dbad(1)*rand);
                if rand>0.5
                    loser=badguys(randbad,1);
                else
                    loser=badguys(randbad,2);
                end
            case 'ranked'
                loser=max(badguys(end,1),badguys(end,1));
        end
        allrois(loser,:)=[];
        
        % now reform the distance matrix and identify badguys
        distmat=squareform(pdist(allrois));
        badones=(distmat<excludedistance);
        clear badguys;
        [badguys(:,1) badguys(:,2)]=find(triu(badones,1));
        badguys=sortrows(badguys,2);
    end
end

% and tell the user what happened
file = sprintf('%s/%s_%s_ex%gmm_%gROIS.txt', pwd, outputname, type, excludedistance, size(allrois,1));xh
dlmwrite(file,allrois,'\t');
fprintf('Cut %d ROIs to %d ROIs\n',startnumrois,size(allrois,1));
fprintf('Writing output to %s\n',file);

