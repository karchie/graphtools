function [BC EBC] = BCEBC(params,analysistype,xdistance,varargin)
% BCEBC Calculate edge and node betweenness centralities
%
% This script calculates edge and node betweenness centralities, on
% unweighted undirected graphs. The node centralities are written to a .txt
% file, the edge centralities are written to a .mat file.
%
% google betweenness centrality if you're unfamiliar with what it is.
%
% USAGE: [BC EBC] = ngt.BCEBC(prmfile,analysistype,xdistance,*nodergb,*edgergb)
% USAGE: [BC EBC] = ngt.BCEBC('modbox.prm','thr',[])
% USAGE: [BC EBC] = ngt.BCEBC('modbox.prm','thr',[],'jet','cool')
%
% params: ngt params file
% analysistype: 'box' or 'thr'
% xdistance: exclusion distance; [] means none
% nodergb: node color (optional, default 'jet')
% edgergb: edge color (optional, default 'jet')

% Author: jdp 10/10/10

clf;
makebinary=1;
nodergb='jet';
edgergb='jet';
if ~isempty(varargin)
    if length(varargin) ~= 2
        error('both node and edge RGB values must be specified');
    end
    nodergb=varargin{1,1};
    edgergb=varargin{1,2};
end

% read in the prmfile settings
params = ngt.load_prm(params);
if ~exist(params.writepath, 'file')
    mkdir(params.writepath);
end

% create filebase for output
filestem = ngt.filenameprep(params,analysistype);

% determine the # thresholds, etc. for the analysis
[subjectarray thresholdarray numanalyses unused] = ngt.matrix_parameter_setter(params,analysistype);

% load the matrix
[matrix nodes unused] = ngt.matfile_loader(params.matfile);

% apply a distance exclusion?
if ~isempty(xdistance)
    if isnumeric(xdistance) && (xdistance>=0)
        matrix = ngt.matrix_xdistance(matrix,params.roifile,xdistance);
        xd=ngt.dotremover(num2str(xdistance));
        filestem=[filestem '_xd' xd ];
    else
        error('xdistance is not >=0 or is not numeric.\n');
    end
end

if makebinary
    filestem = [filestem '_BI' ];
end

% initialize matrices and prepare output directory
EBC=zeros(nodes,nodes,numanalyses);
BC=zeros(nodes,numanalyses);
filestem=[filestem '_BCEBC' ];
outdir=[params.writepath '/' filestem];
if ~exist(outdir, 'file')
    mkdir(outdir);
end

for i=1:numanalyses
    fprintf('Thr/box %d\n',i);
    
    % get the appropriate matrix
    rmat = ngt.matrix_former(matrix,subjectarray(i,1),subjectarray(i,2),'2D','diagout');
    
    % threshold the matrix
    [rmat unused unused] = ngt.matrix_thresholder(rmat,thresholdarray(i,1),params.thresholdtype);
    
    % if user wants binarized networks
    if makebinary
        rmat = single(0 < rmat);
    end
    
    % find betweeness
    [EBC(:,:,i) BC(:,i)] = ngt.edge_betweenness_bin(rmat);
end

% save BC results
dlmwrite([outdir '/' filestem '_BC.txt'],BC,'\t')

% save EBC in a .mat file
save([outdir '/' filestem '_EBC.mat'],'EBC','BC');

% write visuals of the BC and EBC values

% gotta map EBC values as rgbs first:
EBCrgb=ngt.matrixrgbmapper(EBC,edgergb,1);
ngt.M_visuals(params,analysistype,[outdir '/' filestem '_BC.txt'],1,nodergb,[],xdistance,0,0,'black',2,15,EBCrgb,1);
