function quickprmfile(name,varargin)
% QUICKPRMFILE Generate a graphtools parameters (.prm) file
% 
% The prmfile contains several specifications: 
% matfile
% roifile
% stem for output
% subjectA
% subjectZ
% lothreshold
% stepthreshold
% hithreshold
% path/to/write/output/to/ 
% boxcarthreshold
% boxcarsize
% boxcarstepsize
% type of threshold (kden - edgedensity, or r - matrix value)
% 
% e.g.: 
% matrix.mat
% roifile.roi
% finalanalysis
% 1 
% 26 
% 0 
% 0.01
% 0.2
% /data/stete/networkanalysis/
% 0.1 
% 10
% 2
% r
%
% USAGE: ngt.quickprmfile(name,*matfile,roifile,stem,subjectA,subjectZ,lothresh,stepthresh,hithresh,heredir,boxthresh,boxcarsize,boxcarstep,type*)
% USAGE: ngt.quickprmfile('dummyprmfile.txt');
% USAGE: ngt.quickprmfile('dummyprmfile.txt','this.mat','this.roi','this',1,11,0,0.01,.1,'/write/stuff/here/',.1,15,5,'r');
%
% AUTHOR: jdp 9/15/10
if ~isempty(varargin)
    argsz = size(varargin);
    argn = argsz(2);
    disp('arguments:')
    disp(argn);
    if argn < 13
        error('Not enough arguments (given %d, need 13)', argn)
    elseif argn > 13
        error('Too many arguments (given %d, need 13)', argn)
    end
    matfile=varargin{1,1};
    roifile=varargin{1,2};
    stem=varargin{1,3};
    subjectA=varargin{1,4};
    subjectZ=varargin{1,5};
    lothresh=varargin{1,6};
    stepthresh=varargin{1,7};
    hithresh=varargin{1,8};
    heredir=varargin{1,9};
    boxthresh=varargin{1,10};
    boxsize=varargin{1,11};
    boxcarstep=varargin{1,12};
    type=varargin{1,13};
else
    matfile='dummy.mat';
    roifile='dummy.roi';
    stem='network';
    subjectA=1;
    subjectZ=1;
    lothresh=0;
    stepthresh=0.01;
    hithresh=.15;
    heredir=pwd;
    boxthresh=0.1;
    boxsize=1;
    boxcarstep=1;
    type='r';
end

fid=fopen(name,'w');
fprintf(fid,'%s\n',matfile);
fprintf(fid,'%s\n',roifile);
fprintf(fid,'%s\n',stem);
fprintf(fid,'%d\n',subjectA);
fprintf(fid,'%d\n',subjectZ);
fprintf(fid,'%f\n',lothresh);
fprintf(fid,'%f\n',stepthresh);
fprintf(fid,'%f\n',hithresh);
fprintf(fid,'%s\n',heredir);
fprintf(fid,'%f\n',boxthresh);
fprintf(fid,'%d\n',boxsize);
fprintf(fid,'%d\n',boxcarstep);
fprintf(fid,'%s\n',type);
fclose(fid);
