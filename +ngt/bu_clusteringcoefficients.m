function [bucc meancc] = bu_clusteringcoefficient(rmat)
% BU_CLUSTERINGCOEFFICIENT Calculate the local clustering coefficient of
%
% After Watts and Strogatz, 1998.
%
% USAGE: [bucc meancc] = bu_clusteringcoefficient(rmat)

d=size(rmat);
bucc=zeros(d(1),1);
for i=1:d(1)
    clear neighbors numneighbors secondaryneighbors;
    neighbors=find(rmat(i,:));
    if ~isempty(neighbors)
        numneighbors=size(neighbors,2);
        if numneighbors>=2
            secondaryneighbors=nnz(rmat(neighbors,neighbors));
            bucc(i,1)=(secondaryneighbors)/(numneighbors^2-numneighbors);
        else
            bucc(i,1)=0;
        end
    else
        bucc(i,1)=0;
    end
end

meancc=mean(bucc);