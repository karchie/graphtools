function basergb = fcimage_2dviewer(fcimg,fcrgbpalette,outputname,varargin)
% FCIMAGE_2DVIEWER Lays one image over a base with a specified RGB palette
% 
% This script takes an image (e.g. module assignments) and lays it over a
% base image, with an rgb palette that you specify. It is called
% automatically by roi_atlas_clrfile in the creation of paint files
% 
% USAGE: rgb = ngt.fcimage_2dviewer(fcimage,rgbpalette,outputname,*baseimage,basergbpalette*)
% USAGE: rgb = ngt.fcimage_2dviewer('col1.4dfp.img','custom.txt','col1.tiff');
% USAGE: rgb = ngt.fcimage_2dviewer('col1.4dfp.img','jet','col1.tiff');
% USAGE: rgb = ngt.fcimage_2dviewer('col1.4dfp.img','custom.txt','col1.tiff','7112L.4dfp.img','gray');
%
% AUTHOR: jdp 11/4/10

ngt.siteconfig
baseimg = fcimage_base_default;

% set a filebase, load the fcimage (if necessary)
if isnumeric(fcimg)
    fcmat=fcimg;
    clear fcimg;
else
    [fcmat fcframes unused] = read_4dfpimg(fcimg);
end

% set the base extension, load the base (default can be overridden)
if isempty(varargin)
    basergbpalette='gray';
    [basemat baseframes unused] = read_4dfpimg(baseimg);
else
    if length(varargin) ~= 2
        error('must provide baseimage and basergbpalette');
    end
    baseimg=varargin{1,1};
    basergbpalette=varargin{1,2};
    if ~isnumeric(baseimg)
        [basemat baseframes unused] = read_4dfpimg(baseimg);
    end
end
    
% make sure the base and fcimage correspond to one another
if (fcframes~=1 || baseframes ~=1)
    error('image_2dviewer: This is for single volumes only!');
end

% rotate and reshape the images for viewing pleasure
[basemat] = reshape_rotate(basemat); % get it in the 3D format
[basemat] = wholebrainimage(basemat); % make it into a 2D picture
[fcmat] = reshape_rotate(fcmat); % get it in the 3D format
[fcmat] = wholebrainimage(fcmat); % make it into a 2D picture

% now color the images
[basergb unused unused unused unused] = ngt.rgbmapper(basemat,0,basergbpalette);
[fcrgb unused unused unused unused] = ngt.rgbmapper(fcmat,0,fcrgbpalette);

% add nonzero fcmat values to the baseimage and write a picture
fcmat=logical(repmat(fcmat,[1 1 3]));
basergb(fcmat)=fcrgb(fcmat);
imwrite(imresize(basergb/255,2,'nearest'),outputname);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function rotated = reshape_rotate(datamat)

% assumes a 1-volume 4dfp

datamat=reshape(datamat,[48 64 48]);
rotated=zeros(64,48,48,'single');
for i=1:48
    rotated(:,:,i)=rot90(datamat(:,:,i),-1);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function wholebrain = wholebrainimage(datamat)

% create a big brain image. makes a 6x8 set of pictures of a volume

dim1=6; % 333 images have 48 slices, make it a 6x8 picture
dim2=8;
npix1=dim1*dim2+1;

k=0;
brainrow=zeros(64,384,'single');
wholebrain=zeros(384,384,'single');
for a=1:dim1 % assuming 333 with 48 vertical slices
    for b=1:dim2
        k=k+1;
        slice=npix1-k;
        if b==1
            brainrow=datamat(:,:,slice);
        else
            brainrow=[brainrow datamat(:,:,slice)];
        end
    end
    if a==1
        wholebrain=brainrow;
    else
        wholebrain=[wholebrain;brainrow];
    end
end

