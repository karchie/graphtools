function mat2pajek(mat,roifile,outputname)
% MAT2PAJEK Converts a matrix and roifile to a Pajek-format graph
%
% This takes a matrix, an roifile, and a name for the output, and writes
% the graph in the pajek format, which is used in Pajek (for windows only),
% and also for some compiled versions of things, like Infomap.
%
% USAGE: ngt.mat2pajek(mat,roifile,outputname)
% USAGE: ngt.mat2pajek(mat,'rois.roi','/data/dolphins.net')
%
% NOTE: pajek files should have .net extensions
%
% AUTHOR: jdp 10/10/10

% read in ROI names from roifile
[xyz name] = ngt.roifilereader(roifile);

% only the upper triangle
mat=triu(mat,1);

% get edges and values
[x y z] = find(mat);

%%% make the input file %%%
[nodes nodes] = size(mat);

c=clock;
fprintf('\t%2.0f:%2.0f:%2.0f: mat2pajek: writing .net file, with %d vertices and %d edges\n',c(4),c(5),c(6),nodes,length(x));

fid=fopen(outputname,'wt');
fprintf(fid,'*Vertices %d\n',nodes);
for j=1:nodes
    fprintf(fid,'%d "%s"\n',j,name{j,1});
end
fprintf(fid,'*Edges %d\n',length(x));
for j=1:length(x);
    fprintf(fid,'%d %d %g\n',x(j),y(j),z(j));
end
fclose(fid);
